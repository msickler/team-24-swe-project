FRONTEND_DIR = frontend

# install dependences
install:
	(cd $(FRONTEND_DIR) && npm install)
	(cd $(FRONTEND_DIR) && npm i react-router-dom)
	(cd $(FRONTEND_DIR) && npm install axios)

# start dev environment without cd into front-end
start:
	(cd $(FRONTEND_DIR) && npm start)

# install depdencies and start dev environment
run:
	$(MAKE) install
	$(MAKE) start


# runs javascript unit tests
jest-test:
	(cd $(FRONTEND_DIR) && npm install)
	(cd $(FRONTEND_DIR) && npx jest src/components/nocarnoproblem.test.js)

# runs selenium tests
test-frontend:
	(cd $(FRONTEND_DIR) && python selenium_test.py)

