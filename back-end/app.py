from flask import Flask, make_response, jsonify, request
from flask_cors import CORS
from flask_restless import APIManager
from dotenv import load_dotenv
import boto3
import os
from sqlalchemy import text
from models import db, config_database, Stops, Destinations, Lines, stops_lines_association

"""
Running the App:
- If file not named app.py, use 'flask --app app run --debug'
"""

load_dotenv()
app = Flask(__name__)
CORS(app)	# Note this makes our app's resources open to all other origins
config_database(app, mock=False)

# Handler for accessing every instance in Stops table
@app.route('/api/stops/all', methods=['GET'])
def get_all_stops():
	all_stops = db.session.query(Stops).all()
	return jsonify([stop.serialize for stop in all_stops])

# Handler for accessing every instance in Lines table
@app.route('/api/lines/all', methods=['GET'])
def get_all_lines():
	all_lines = db.session.query(Lines).all()
	return jsonify([line.serialize for line in all_lines])

# Handler for accessing every instance in Destinations table
@app.route('/api/destinations/all', methods=['GET'])
def get_all_destinations():
	all_destinations = db.session.query(Destinations).all()
	return jsonify([dest.serialize for dest in all_destinations])

# API Use Note:
# You can have multiple sorting and filtering arguments 
# Example: /api/stops?wheelchair=0&sort_by=name

# Handler for Lines Filtering and Sorting
@app.route('/api/lines', methods=['GET'])
def get_lines_query():

	# getting values of filtering arguments
	high_frequency = request.args.get('high_frequency')
	types = request.args.get('types')
	colors = request.args.get('colors')
	modes = request.args.get('modes')

	# getting values of sorting arguments
	sort_by = request.args.get('sort_by')
	sort_reversed = request.args.get('sort_reversed')

	# convert sort_reversed to boolean
	sort_reversed = bool(int(sort_reversed)) if sort_reversed else False

	# getting all lines
	lines = db.session.query(Lines).all()

	# filter
	if high_frequency: # /api/lines?high_frequency=0 or /api/lines?high_frequency=0,1
		lines = [line for line in lines if line.is_high_frequency in list(map(int, high_frequency.split(',')))]
	if types: # /api/lines?types=Local or /api/lines?types=Local, Flyer
		lines = [line for line in lines if str(line.route_type) in types]
	if colors: # api/lines?colors=%23E2231A or /api/lines?colors=%23E2231A, %23004A97
		lines = [line for line in lines if line.route_color in colors]
	if modes:  # /api/lines?modes=bus or /api/lines?modes=bus,regionalTrain
		lines = [line for line in lines if line.mode_name in modes]

	# sort
	if sort_by == "name": # /api/lines?sort_by=name or api/lines?sort_by=name&sort_reversed=1
		lines = sorted(lines, key = lambda line: line.route_long_name, reverse = sort_reversed)
	elif sort_by == "code": # /api/lines?sort_by=code or api/lines?sort_by=code&sort_reversed=1
		lines = sorted(lines, key = lambda line: int(line.route_short_name), reverse = sort_reversed)

	# return sorted and filtered data
	lines_data = [line.serialize for line in lines]
	return jsonify(lines_data)

# Handler for Stops Filtering and Sorting
@app.route('/api/stops', methods=['GET'])
def get_stops_query():

	# getting values of sorting arguments
	sort_by = request.args.get('sort_by')
	sort_reversed = request.args.get('sort_reversed')

	# convert sort_reversed to boolean
	sort_reversed = bool(int(sort_reversed)) if sort_reversed else False

	# getting all stops
	stops = db.session.query(Stops).all()

	# sort
	if sort_by == "name":
		stops = sorted(stops, key = lambda stop: stop.stop_name, reverse = sort_reversed)
	elif sort_by == "id": # /api/stops?sort_by=id or /api/stops?sort_by=id&sort_reversed=1
		stops = sorted(stops, key = lambda stop: stop.global_stop_id, reverse = sort_reversed)
	elif sort_by == "latitude": # /api/stops?sort_by=latitude or /api/stops?sort_by=latitude&sort_reversed=1
		stops = sorted(stops, key = lambda stop: stop.stop_lat, reverse = sort_reversed)
	elif sort_by == "longitude": # /api/stops?sort_by=longitude or /api/stops?sort_by=longitude&sort_reversed=1
		stops = sorted(stops, key = lambda stop: stop.stop_lon, reverse = sort_reversed)
	elif sort_by == "num_lines": # /api/stops?sort_by=num_lines or /api/stops?sort_by=num_lines&sort_reversed=1
		# select all records from the stops_lines_association table
		stops_and_lines = db.session.execute(stops_lines_association.select()).fetchall()

		# use a dictionary to store the count of each stop_code
		stop_counts = {}
		for entry in stops_and_lines:
			stop_code = entry.stop_code
			stop_counts[stop_code] = stop_counts.get(stop_code, 0) + 1

		# sort the stops based on the count of their stop_code
		stops = sorted(stops, key=lambda stop: stop_counts.get(stop.stop_code, 0), reverse=sort_reversed)

	# return sorted data
	stops_data = [stop.serialize for stop in stops]
	return jsonify(stops_data)

# Handler for Destination Filtering and Sorting
@app.route('/api/destinations', methods=['GET'])
def get_dests_query():
	
	# getting values of filtering arguments
	business_statuses = request.args.get('business_statuses')
	open = request.args.get('open')

	# getting values of sorting arguments
	sort_by = request.args.get('sort_by')
	sort_reversed = request.args.get('sort_reversed')

	# convert sort_reversed to boolean
	sort_reversed = bool(int(sort_reversed)) if sort_reversed else False

	# getting all destinations
	dests = db.session.query(Destinations).all()

	# filter
	if business_statuses: # /api/destinations?business_statuses=OPERATIONAL or /api/destinations?business_statuses=OPERATIONAL, CLOSED_TEMPORARILY
		dests = [dest for dest in dests if dest.business_status in business_statuses]

	# sort
	if sort_by == "name": # /api/destinations?sort_by=name or /api/destinations?sort_by=name&sort_reversed=1
		dests = sorted(dests, key = lambda dest: dest.name, reverse = sort_reversed)
	elif sort_by == "latitude": # /api/destinations?sort_by=latitude or /api/destinations?sort_by=latitude&sort_reversed=1
		dests = sorted(dests, key = lambda dest: dest.location_lat, reverse = sort_reversed)
	elif sort_by == "longitude": # /api/destinations?sort_by=longitude or /api/destinations?sort_by=longitude&sort_reversed=1
		dests = sorted(dests, key = lambda dest: dest.location_lon, reverse = sort_reversed)
	elif sort_by == "ratings": # /api/destinations?sort_by=ratings or /api/destinations?sort_by=ratings&sort_reversed=1
		def rating_func(dest):
			# Account for destinations with no rating
			return dest.rating if dest.rating != None else 0
		dests = sorted(dests, key = rating_func, reverse = sort_reversed)

	# return sorted and filtered data
	dests_data = [dest.serialize for dest in dests]
	return jsonify(dests_data)

# Handler for Lines Search Queries
@app.route('/api/lines/search', methods=['GET'])
def search_lines():
	"""
	Example Usage: api/lines/search?query=lamar
	"""

	search_query = request.args.get('query', '')
	if search_query:
		connection = db.engine.connect()
		# sql instruction matches substring to global_route_id, mode_name, route_long_name, route_short_name, and route_color columns
		sql_query = text("SELECT * FROM ncnp.lines WHERE global_route_id LIKE :global_route_id OR mode_name LIKE :mode_name OR route_long_name LIKE :route_long_name OR route_short_name LIKE :route_short_name OR route_color LIKE :route_color")
		params = {
			'global_route_id': "%" + search_query + "%",
			'mode_name': "%" + search_query + "%",
			'route_long_name': "%" + search_query + "%",
			'route_short_name': "%" + search_query + "%",
			'route_color': "%" + search_query + "%"
		}
		result = connection.execute(sql_query, params)
		data = result.fetchall()
		lines = [line._asdict() for line in data]
		return jsonify(lines)
	else:
		return jsonify({"error": "No search query provided"}), 400

# Handler for Stops Search Queries
@app.route('/api/stops/search', methods=['GET'])
def search_stops():
	"""
	Example Usage: api/stops/search?query=Rio
	"""

	search_query = request.args.get('query', '')
	if search_query:
		connection = db.engine.connect()
		# sql instruction matches substring to global_stop_id, stop_code, location_type, stop_name, and wheelchair_boarding columns
		sql_query = text("SELECT * FROM ncnp.stops WHERE global_stop_id LIKE :global_stop_id OR stop_code LIKE :stop_code OR location_type LIKE :location_type OR stop_name LIKE :stop_name OR wheelchair_boarding LIKE :wheelchair_boarding")
		params = {
			'global_stop_id': "%" + search_query + "%",
			'stop_code': "%" + search_query + "%",
			'location_type': "%" + search_query + "%",
			'stop_name': "%" + search_query + "%",
			'wheelchair_boarding': "%" + search_query + "%"
		}
		result = connection.execute(sql_query, params)
		data = result.fetchall()
		stops = [stop._asdict() for stop in data]
		return jsonify(stops)
	else:
		return jsonify({"error": "No search query provided"}), 400

# Handler for Destinations Search Queries
@app.route('/api/destinations/search', methods=['GET'])
def search_destinations():
	"""
	Example Usage: api/destinations/search?query=library
	"""
	
	search_query = request.args.get('query', '')
	if search_query:
		connection = db.engine.connect()
		# sql instruction matches substring to name, business_status, address, phone, and opening_hours columns
		sql_query = text("SELECT * FROM ncnp.destinations WHERE name LIKE :name OR business_status LIKE :business_status OR address LIKE :address OR opening_hours LIKE :opening_hours OR phone LIKE :phone OR rating LIKE :rating")
		params = {
			'name': "%" + search_query + "%",
			'business_status': "%" + search_query + "%",
			'address': "%" + search_query + "%",
			'opening_hours': "%" + search_query + "%",
			'phone': "%" + search_query + "%",
			'rating': "%" + search_query + "%"
		}
		result = connection.execute(sql_query, params)
		data = result.fetchall()
		dests = [dest._asdict() for dest in data]
		return jsonify(dests)
	else:
		return jsonify({"error": "No search query provided"}), 400
	
with app.app_context():
	# Make sure our tables are present in the database
	db.create_all()

	# Use Restless-NG to make our single instance API Endpoints
	manager = APIManager(app=app, session=db.session)
	manager.create_api(Stops, methods=['GET'])
	manager.create_api(Lines, methods=['GET'])
	manager.create_api(Destinations, methods=['GET'])

if __name__ == "__main__":
	app.run(port=5000, debug=True)

