from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
import boto3
import os
import json
import requests

from models import db, config_database, Stops
from sqlalchemy import inspect

if __name__ == '__main__':
	# Setup stuff
	load_dotenv()
	places_key = os.environ.get('PLACES_API_KEY')
	app = Flask('Add_Stop_Images')
	with app.app_context():
        
		config_database(app, mock=False)

		print("Starting stop image population...")
		
		all_stops = Stops.query.all()
		print("Queried", len(all_stops), "stops")
		for stop in all_stops:
			print("Populating Stop #", stop.stop_code)
			# Use lat and long to query MAPSAPI and get our img
			lat = stop.stop_lat
			lon = stop.stop_lon

			# Put img url into new column
			stop.stop_img = f"https://maps.googleapis.com/maps/api/streetview?size=400x400&location={lat},{lon}&fov=80&heading=70&key={places_key}"
			db.session.merge(stop)

		print("Committing session!")
		db.session.commit()

	# lat = stop.get('stop_lat')
	# 			lon = stop.get('stop_lon')
	# 			street_view_url = f"https://maps.googleapis.com/maps/api/streetview?size=400x400&location={lat},{lon}&fov=80&heading=70&key={places_key}"
