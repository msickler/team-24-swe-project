from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
import boto3
import os
import json
import requests

from models import db, config_database, Stops, Destinations, Lines

if __name__ == '__main__':
	load_dotenv()
	transit_key = os.environ.get('TRANSIT_API_KEY')
	here_key = os.environ.get('HERE_API_KEY')
	places_key = os.environ.get('PLACES_API_KEY')
	transit_endpoint = 'https://external.transitapp.com/v3/public/'
	here_endpoint = 'https://transit.hereapi.com/v8/departures'
	places_ns_endpoint = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json'
	places_p_endpoint = 'https://maps.googleapis.com/maps/api/place/details/json'

	app = Flask('Populate')
	config_database(app, mock=False)

	# Load scraped stops data
	with open('../../data/stops.json', 'r') as f:
		stops_data = json.load(f)

	with app.app_context(), db.session.no_autoflush:
		db.create_all()

		# Iterate through JSON stops array and populate Stops table
		# num_stops = 100
		stops_so_far = 0
		reversed_stops = reversed(stops_data['stops'])

		for stop in reversed_stops:
			lat = stop.get('stop_lat')
			lon = stop.get('stop_lon')
			street_view_url = f"https://maps.googleapis.com/maps/api/streetview?size=400x400&location={lat},{lon}&fov=80&heading=70&key={places_key}"
			map_url = f"https://maps.google.com/maps?q=${lat},${lon}&hl=es;z=14&output=embed"
			new_stop = Stops(
				global_stop_id=stop.get('global_stop_id'),
				location_type=str(stop.get('location_type')),
				stop_code=int(stop.get('stop_code')),
				stop_lat=lat,
				stop_lon=lon,
				stop_name=stop.get('stop_name'),
				wheelchair_boarding=stop.get('wheelchair_boarding'),
				stop_image_url = street_view_url,
				stop_map_url = map_url
			)
			print("Populating stop", int(stop.get('stop_code')))

			db.session.add(new_stop)

			# Request lines connected to this bus stop
			radius = 200	# meters of precision, may need to make bigger but dont want to capture more stops
			params = {
				'in' : f'{lat},{lon};r={radius}',
				'apiKey' : here_key,
				'maxPerTransport' : '1',
			}
			here_response = requests.get(here_endpoint, params=params)
			if here_response.status_code != 200:
				print(f"Failed to get lines data: {here_response.content}")
				break

			# Parse lines response
			if len(here_response.json()['boards']) < 1:
				# This means HERE API had no listed departures for this stop
				# so we can't get any connected lines. Skipping this stop.
				print("Skipping stop",new_stop)
				db.session.expunge(new_stop)
				continue

			departures = here_response.json()['boards'][0]['departures']
			added_lines = set()
			for departure in departures:
				line = departure['transport']
				id = int(line.get('name'))
				if id not in added_lines:
					# Check if line with the same global_route_id already exists
					existing_line = Lines.query.filter_by(global_route_id=id).first()

					if existing_line:
						# If it exists, use it
						new_line = db.session.merge(existing_line)
					else:
						# Otherwise, create a new one
						new_line = Lines(
							global_route_id=id,
							mode_name=line.get('mode'),
							route_long_name=line.get('longName'),
							route_short_name=line.get('shortName'),
							route_color=line.get('color'),
							route_text_color=line.get('textColor')
						)

					# Connect this line to our stop model
					new_stop.lines.append(new_line)
					added_lines.add(id)

			# Request destinations around our stop
			radius = 805	# Half a mile
			params = {
				'key' : places_key,
				'location' : f'{lat}, {lon}',
				'radius' : radius,
				'type' : 'point_of_interest',
				'rankby' : 'prominence'
			}
			places_response = requests.get(places_ns_endpoint, params=params)
			if places_response.status_code != 200:
				print(f"Failed to get places data : {places_response.content}")
				break

			# Parse destination response
			destinations = places_response.json()['results']
			dest_so_far = 0
			for dest in destinations:
				# Request more destination details
				google_place_id = dest.get('place_id')
				params = {
					'key' : places_key,
					'place_id' : google_place_id
				}
				places_details_response = requests.get(places_p_endpoint, params=params)
				if places_details_response.status_code != 200:
					print(f"Failed to get places detailed data: {places_details_response.content}")
					break

				# Parse destination details
				dest = places_details_response.json()['result']
				
				existing_dest = Destinations.query.filter_by(google_place_id=google_place_id).first()

				if existing_dest:
					# If it exists, use it
					new_dest = db.session.merge(existing_dest)
				else:
					# Otherwise, create a new one
					new_dest = Destinations(
						# place_id=our_place_id,
						google_place_id=google_place_id, 
						name=dest.get('name'), 
						business_status=dest.get('business_status'), 
						location_lat=dest.get('geometry', {}).get('location', {}).get('lat'),
						location_lon=dest.get('geometry', {}).get('location', {}).get('lng'),
						address=dest.get('formatted_address'), 
						phone=dest.get('formatted_phone_number'),
						opening_hours=dest.get('opening_hours', {}).get('weekday_text'),
						reviews=dest.get('reviews'),
						icon=dest.get('icon'),
						photos=dest.get('photos')
						)
				
				# # Connect this destination to other models
				new_stop.destinations.append(new_dest)

				for line in new_stop.lines:
					line.destinations.append(new_dest)

				dest_so_far += 1
				if dest_so_far >= 5:
					break

			# Commit transaction to db
			db.session.commit()

			stops_so_far += 1
			# if stops_so_far >= num_stops:
			# 	break
