from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
import os

db = SQLAlchemy()

def config_database(app, mock=False):
	"""
	Initializes SQLAlchemy and sets up our database
	"""
	load_dotenv()

	if mock:
		# Creating my own database for the purposes of testing!
		app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///mock_db.db'
	else:
		# Fetch environment variables from .env file
		username = os.environ.get('DB_USERNAME')
		password = os.environ.get('DB_PASSWORD')
		aws_rds_endpoint = os.environ.get('AWS_RDS_ENDPOINT')
		db_name = os.environ.get('DB_NAME')

		# Using AWS database 
		app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql://{username}:{password}@{aws_rds_endpoint}:3306/{db_name}'
	
	db.init_app(app)

# Define our table models
class Stops(db.Model):
	global_stop_id = db.Column(db.String(45))	# Originally a string, global_stop_id
	location_type = db.Column(db.String(45))
	stop_code = db.Column(db.Integer, primary_key=True)	# Originally a string
	stop_lat = db.Column(db.Float)
	stop_lon = db.Column(db.Float)
	stop_name = db.Column(db.String(45))
	stop_img = db.Column(db.String(255))
	wheelchair_boarding = db.Column(db.Integer)
	stop_img = db.Column(db.String(255))
	map_url = db.Column(db.String(255))


	# Connected models
	destinations = db.relationship('Destinations', 
									secondary='stops_destinations_association', 
									backref=db.backref('stops', lazy='dynamic'))
	lines = db.relationship('Lines', 
									secondary='stops_lines_association', 
									backref=db.backref('stops', lazy='dynamic'))
	
	@property
	def serialize(self):
		return {
			'global_stop_id': self.global_stop_id,
			'location_type': self.location_type,
			'stop_code': self.stop_code,
			'stop_lat': self.stop_lat,
			'stop_lon': self.stop_lon,
			'stop_name': self.stop_name,
			'wheelchair_boarding': self.wheelchair_boarding,
			'stop_img' : self.stop_img,
			'map_url' : self.map_url,
		}


class Destinations(db.Model):
	place_id = db.Column(db.Integer, primary_key=True)	# We need to make this
	google_place_id = db.Column(db.String(45))
	name = db.Column(db.String(45))
	business_status = db.Column(db.String(45))
	location_lat = db.Column(db.Float)
	location_lon = db.Column(db.Float)
	address = db.Column(db.String(300))
	phone = db.Column(db.String(20))
	opening_hours = db.Column(db.JSON)
	reviews = db.Column(db.JSON)
	icon = db.Column(db.String(500)) 	# (link to image?)
	photos = db.Column(db.JSON)			# (link to image?)
	map_url = db.Column(db.String(255))
	photo_urls = db.Column(db.String(255))
	rating = db.Column(db.Float)



	# Connected models
	# stops
	lines = db.relationship('Lines', 
									secondary='destinations_lines_association', 
									backref=db.backref('destinations', lazy='dynamic'))
	
	@property
	def serialize(self):
		return {
			'place_id': self.place_id,
			'google_place_id': self.google_place_id,
			'name': self.name,
			'business_status': self.business_status,
			'location_lat': self.location_lat,
			'location_lon': self.location_lon,
			'address': self.address,
			'phone': self.phone,
			'opening_hours': self.opening_hours,
			'reviews': self.reviews,
			'icon': self.icon,
			'photos': self.photos,
			'map_url': self.map_url,
			'photo_urls': self.photo_urls,
			'rating': self.rating
		}


class Lines(db.Model):
	global_route_id = db.Column(db.Integer, primary_key=True)
	mode_name = db.Column(db.String(45))
	route_long_name = db.Column(db.String(45))
	route_short_name = db.Column(db.String(45))
	route_color = db.Column(db.String(45))
	route_text_color = db.Column(db.String(45))
	route_type = db.Column(db.String(45))
	is_high_frequency = db.Column(db.Boolean)
	highlighted_stop = db.Column(db.String(45))
	highlighted_img = db.Column(db.String(255))
	map_url = db.Column(db.String(45))

	# Connected models
	# destinations
	# stops

	@property
	def serialize(self):
		# We can assume we have at least one stop
		stop:Stops = self.stops[0]
		image = stop.stop_img

		return {
			'global_route_id': self.global_route_id,
			'mode_name': self.mode_name,
			'route_long_name': self.route_long_name,
			'route_short_name': self.route_short_name,
			'route_color': self.route_color,
			'route_text_color': self.route_text_color,
			'highlighted_stop_img': image,
			'route_type': self.route_type,
			'is_high_frequency': self.is_high_frequency,
			'highlighted_stop': self.highlighted_stop,
			'highlighted_img': self.highlighted_img,
			'map_url': self.map_url,
		}


# Define our join tables
stops_destinations_association = db.Table(
	'stops_destinations_association', 
	db.Column('stop_code', db.Integer, db.ForeignKey('stops.stop_code')),
	db.Column('place_id', db.Integer, db.ForeignKey('destinations.place_id'))
)


stops_lines_association = db.Table(
	'stops_lines_association', 
	db.Column('stop_code', db.Integer, db.ForeignKey('stops.stop_code')),
	db.Column('global_route_id', db.Integer, db.ForeignKey('lines.global_route_id'))
)


destinations_lines_association = db.Table(
	'destinations_lines_association',
	db.Column('place_id', db.Integer, db.ForeignKey('destinations.place_id')),
	db.Column('global_route_id', db.Integer, db.ForeignKey('lines.global_route_id'))
)