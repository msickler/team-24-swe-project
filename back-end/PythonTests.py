import unittest

import requests

URL = "https://backend.nocarnoproblem.net/api"

GOOGLEURL = "https://maps.googleapis.com/maps/api/place/photo?photo_reference=AcJnMuHL5w--KmuEo6CB7_S3CgIhpoXlV0LvAH5CN0a5gJGpArRw1PSVgRuZLbGSuHN_PZTMMJCtJdSUk7o3R8fAjQY7c8eAJCUym18Yg7zUxxnGAk9k41-A7hHZ6o_HL7-AYpkifvbwZopkoNn-md-uLKztTj7NjNmgUKAc4XN-gqmuSEXe&maxheight=300&key=AIzaSyDIhQ1fhbJ1UXfLAZxHTecmRLWAvBDctFE"

#Run tests: python -m unittest PythonTests
class Test(unittest.TestCase):

    def setup(self):
        response = requests.get(URL)

    def test0(self):
        headers = {
            'Accept': 'application/vnd.api+json'
        }
        response = requests.get(f'{URL}/stops', headers=headers)
        self.assertEqual(response.status_code, 200)
        res_json_count = len(response.json())
        self.assertEqual(res_json_count, 2297)


    def test1(self):
        headers = {
            'Accept': 'application/vnd.api+json'
        }
        response = requests.get(f'{URL}/lines', headers=headers)
        self.assertEqual(response.status_code, 200)
        res_json_count = len(response.json())
        self.assertEqual(res_json_count, 70)
    
    def test2(self):
        headers = {
            'Accept': 'application/vnd.api+json'
        }
        response = requests.get(f'{URL}/destinations', headers=headers)
        self.assertEqual(response.status_code, 200)
        res_json_count = len(response.json())
        self.assertEqual(res_json_count, 1805)
    
    
    def test3(self):
        headers = {
            'Accept': 'application/vnd.api+json'
        }
        response = requests.get(f'{URL}/stops/628', headers=headers)
        self.assertEqual(response.status_code, 200)
        # Check that we got the correct instance
        name = response.json()['data']['attributes']['stop_name']
        self.assertEqual(name, "Lamar / Carpenter")
    
    def test4(self):
        headers = {
            'Accept': 'application/vnd.api+json'
        }
        response = requests.get(f'{URL}/stops/243', headers=headers)
        self.assertEqual(response.status_code, 200)
        # Check that we got the correct instance
        name = response.json()['data']['attributes']['stop_name']
        self.assertEqual(name, "Rio Grande / 15th")
    
    def test5(self):
        headers = {
            'Accept': 'application/vnd.api+json'
        }
        response = requests.get(f'{URL}/lines/1', headers=headers)
        self.assertEqual(response.status_code, 200)
        # Check that we got the correct instance
        name = response.json()['data']['attributes']['route_long_name']
        self.assertEqual(name, "1-North Lamar/South Congress")

    def test6(self):
        headers = {
            'Accept': 'application/vnd.api+json'
        }
        response = requests.get(f'{URL}/lines/2', headers=headers)
        self.assertEqual(response.status_code, 200)
        # Check that we got the correct instance
        name = response.json()['data']['attributes']['route_long_name']
        self.assertEqual(name, "2-Rosewood/CESAR CHAVEZ")
    
    def test7(self):
        headers = {
            'Accept': 'application/vnd.api+json'
        }
        response = requests.get(f'{URL}/destinations/1', headers=headers)
        self.assertEqual(response.status_code, 200)
        # Check that we got the correct instance
        name = response.json()['data']['attributes']['name']
        self.assertEqual(name, "Sustainable Food Center")
    
    def test8(self):
        headers = {
            'Accept': 'application/vnd.api+json'
        }
        response = requests.get(f'{URL}/destinations/2', headers=headers)
        self.assertEqual(response.status_code, 200)
        # Check that we got the correct instance
        name = response.json()['data']['attributes']['name']
        self.assertEqual(name, "Eastside Early College High School")
    
    def test9(self):
        response = requests.get(GOOGLEURL)
        self.assertEqual(response.status_code, 200)

if __name__ == "__main__":
    unittest.main()
