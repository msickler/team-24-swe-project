from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
import boto3
import os
import json
import requests

from models import db, config_database, Stops, Destinations, Lines

if __name__ == '__main__':
    # Set up environment to connect to APIs
    load_dotenv()
    transit_key = os.environ.get('TRANSIT_API_KEY')
    here_key = os.environ.get('HERE_API_KEY')
    places_key = os.environ.get('PLACES_API_KEY')
    transit_endpoint = 'https://external.transitapp.com/v3/public/'
    here_endpoint = 'https://transit.hereapi.com/v8/departures'
    places_ns_endpoint = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json'
    places_p_endpoint = 'https://maps.googleapis.com/maps/api/place/details/json'
    streetview_endpoint = 'https://maps.googleapis.com/maps/api/streetview'

    # Create Flask app and database inside it 
    app = Flask('Update_Lists')
    config_database(app, mock=False)

    # Loop through each line and update the following info:
    # - Find random connected stop and add as new column to lines
    # - Create new column with route type
    # - Create a new column with high_frequency

    # Get all lines in database
    with app.app_context():
        all_lines = db.session.query(Lines).all()

        for line in all_lines :
            id = line.global_route_id

            if id < 100 :
                line.route_type = 'Local'
            elif id < 200 :
                line.route_type = 'Flyer'
            elif id < 300 :
                line.route_type = 'Feeder'
            elif id < 400 :
                line.route_type = 'Crosstown'
            elif id < 500 :
                line.route_type = 'Special Service'
            elif id > 600 and id < 700 :
                line.route_type = 'UT Shuttle'
            elif id > 800 and id < 900 :
                line.route_type = 'Rapid'

            hf_ids = [2, 4, 7, 10, 18, 20, 300, 311, 325, 333, 335, 801, 803]
            if id in hf_ids :
                line.is_high_frequency = True
            else :
                line.is_high_frequency = False

            highlight_stop = line.stops.first()
            line.highlighted_stop = highlight_stop.stop_code

            streetview_size = '?size=400x400'
            streetview_fov = '&fov=120'
            streetview_key = '&key=AIzaSyDIhQ1fhbJ1UXfLAZxHTecmRLWAvBDctFE'
            streetview_loc = '&location=' + str(highlight_stop.stop_lat) + ',' + str(highlight_stop.stop_lon)

            line.highlighted_img = streetview_endpoint + streetview_size + streetview_fov + streetview_key + streetview_loc
            
        db.session.commit()

    # Loop through each stop
    # - Use StreetView API to populate images

    # Loop through Destinations
    # - Update ratings for sorting 