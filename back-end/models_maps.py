from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
import boto3
import os
import json
import requests

from models import db, config_database, Stops, Lines, Destinations
from sqlalchemy import inspect

if __name__ == '__main__':
	# Setup stuff
	load_dotenv()
	places_key = os.environ.get('PLACES_API_KEY')
	app = Flask('Add_Maps_Models')
	with app.app_context():
        
		config_database(app, mock=False)

		print("Starting stop map population...")
		
		all_stops = Stops.query.all()
		all_lines = Lines.query.all()
		all_dests = Destinations.query.all()
		print("Queried", len(all_stops), "stops")
		for stop in all_stops:
			lat = stop.stop_lat
			lon = stop.stop_lon

			# Put map url into new column
			stop.map_url = f"https://maps.google.com/maps?q={lat},{lon}&hl=es;z=14&output=embed"
			db.session.merge(stop)
		
		print("Queried", len(all_lines), "lines")
		for line in all_lines:
			# Get the stop code for the highlighted stop
			highlighted_stop_code = line.highlighted_stop
			
			# Query the Stops table for the stop with the highlighted_stop_code
			highlighted_stop = Stops.query.filter_by(stop_code=highlighted_stop_code).first()
			
			# Check if the highlighted stop exists to avoid errors
			if highlighted_stop:
				line_lat = highlighted_stop.stop_lat
				line_lon = highlighted_stop.stop_lon
				
				# Put map url into new column for the line
				line.map_url = f"https://maps.google.com/maps?q={line_lat},{line_lon}&hl=es;z=14&output=embed"
				db.session.merge(line)
		
		print("Queried", len(all_dests), "dests")
		for dest in all_dests:
			dest_lat = dest.location_lat
			dest_lon = dest.location_lon
			photo_ref = ""
			#check to make sure des.photos is present
			if dest.photos and len(dest.photos) > 0:
				photo_ref = dest.photos[0].get('photo_reference', "")

			dest.map_url = f"https://maps.google.com/maps?q={dest_lat},{dest_lon}&hl=es;z=14&output=embed"
			#Remove Key
			dest.photo_urls = f"https://maps.googleapis.com/maps/api/place/photo?photo_reference={photo_ref}&maxheight=300&key=AIzaSyDIhQ1fhbJ1UXfLAZxHTecmRLWAvBDctFE"
			db.session.merge(dest)

		print("Committing session!")
		db.session.commit()
