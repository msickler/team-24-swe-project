## Team 24 SWE Project

<!--RUBRIC REQUIREMENTS ARE IN COMMENTS (taken from Downing's Ed
Discussion post) -->

**Canvas / Ed Discussion Group 24**
<!-- Canvas / Ed Discussion group number (please check this carefully) -->


## Team Members
<!-- names of the team members -->


- Megan Sickler
- Jose Alonso Rodriguez 
- Jared Kahl
- Zach Cramer
- Trish Truong


## Name of Project: No Car. No Problem.
<!-- name of the project (alphanumeric, no spaces, max 32 chars; this will also be your URL) -->

Proposed project: Americans without cars are often underserved due to the lack of public transportation in American cities. Through this website, we hope to support those without cars in Austin by relating bus lines, bus stops, and popular locations near bus stops. This will help organize bus information, making it easier for those without cars to move around the city.
<!-- the proposed project -->

**Data Source URLs**
<!-- URLs of at least three data sources that you will programmatically scrape using a RESTful API (be very sure about this) -->


- Transit API for Bus/Train Stops: https://transitapp.com/apis
- HERE API for Bus/Train Lines: https://developer.here.com/documentation/public-transit/api-reference-swagger.html
- Google Maps API for Popular Places: https://developers.google.com/maps/documentation/places/web-service


**List of Models**
<!-- at least three models -->


- Bus/Train Lines
- Bus/Train Stops
- Locations in Austin (reachable by public transport)


**Estimated Instances of Each Model**
<!-- an estimate of the number of instances of each model -->


- model 1 estimate: 70 
- model 2 estimate: 2300
- model 3 estimate: 150

## Attributes of Each Model
<!-- each model must have many attributes -->


**Model 1: Bus/Train Lines**
1.  Global route ID (identifies a route across all GTFS data)
2.  Itineraries
3.  Route Long Name
4.  Route Short Name
5.  Route Type (subway, rail, bus, etc.)
6.  Route Color & Route Text Color

**Model 2: Bus/Train Stops**
1.  Notices (a list of any current issues with the station)
2.  Name of location
3.  ID
4.  Code
5.  Location type (entrance to station, routable stop)
6.  Whether it is wheelchair accessible

**Model 3: Places**
1.  Photos
2.  Rating & Reviews
3.  Status (Operational, Closed Temporarily, Closed Permanently)
4.  Hours
5.  Address
6.  Phone Number
7.  Icon

## Attributes of Each Model that can Be Filtered/Sorted
<!-- describe five of those attributes for each model that you can filter or sort -->


**Model 1: Bus/Train Lines**
1.  Global route ID (identifies a route across all GTFS data)
2.  Itineraries
3.  Route Long Name
5.  Route Type (subway, rail, bus, etc.)
6.  Route Color & Route Text Color
8. Location type (entrance to station, routable stop)

**Model 2: Bus/Train Stops**
1.  Notices (a list of any current issues with the station)
2.  Name of location
3.  ID
4.  Code
5.  Location type (entrance to station, routable stop)
6.  Whether it is wheelchair accessible
7. Distance from location

**Model 3: Location**
1.  Photos
2.  Rating & Reviews
3.  Status (Operational, Closed Temporarily, Closed Permanently)
4.  Hours
5.  Address
6.  Phone Number

## Connection Between Instances of Each Model
<!-- instances of each model must connect to instances of at least two other models -->


**Bus/Train Lines**:
Bus/Train Lines will contain instances of connected bus/train stops on those lines as well as instances of nearby popular places that can be traveled to using those lines.

**Bus/Train Stops**:
Bus/Train Stops will contain instances of lines that run to that stop as well as instances of nearby popular places that can be walked to from that stop.

**Locations Reachable**:
Places will contain a list of instances of the closest bus/train stops as well as the instances of bus/train lines that run to those stops.
## Media Within Instances of Each Model
<!-- 
instances of each model must be rich with different media (e.g., feeds, images, maps, text, videos, etc.) (be very sure about this)


describe two types of media for instances of each model
-->
**Bus/Train Lines**:
- A logo for the route incorporating the route color and route text color supplied by the API (i.e. the number 801 in white text with a blue background for Austin’s 801 route)
- An image indicating the route type (subway, rail, bus, etc.)
- Text identifying the route’s long and short names

**Bus/Train Stops**:
- A feed of notices regarding a certain bus stop
- An icon denoting wheelchair accessibility
- Photos of the bus stop using Google’s ‘Places’ API


**Location Reachable**:
- Pictures of the location (taken from maps API)
- Location’s ratings (if available)
- Text description of the location detailing overall summary along with identifying information: contact information (phone number, email address), hours, and address


## Questions Our Site Answers
<!--describe three questions that your site will answer -->
1. In Austin, what locations/places are reachable near certain bus stops?
2. What mode of transportation is available to reach a desired destination?
3. For a given public transport route, what stops are available to be taken?
