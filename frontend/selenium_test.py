from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

from time import sleep


# Set options for not prompting DevTools information
options = Options()
options.add_argument('--headless')
options.add_argument('--disable-dev-shm-usage')
options.add_experimental_option("excludeSwitches", ["enable-logging"])
options.add_argument('--no-sandbox')

print("Selenium testing started")
driver = webdriver.Chrome(options=options)
driver.set_window_size(1440, 900)
driver.get("http://nocarnoproblem.net")
sleep(3)

# test 0: title test
title = driver.title
assert "No Car. No Problem." in title
print("TEST 0: title test passed")

# tests 1-4: page title tests
pageTitles = ["Lines", "Stops", "Destinations", "About"]
for i in range(0,4) :
    navBarLinks = driver.find_elements(By.CLASS_NAME, "link")
    navBarLinks[i].click()
    sleep(3)
    pageTitle = driver.find_element(By.CLASS_NAME, "page-title")
    assert pageTitles[i] in pageTitle.text
    print(f"TEST {i * 2 + 1}: {pageTitles[i]} page title test passed")
    cardContainer = None
    if (i < 3) :
        cardContainer = driver.find_element(By.CLASS_NAME, "card-container")
    else :
        cardContainer = driver.find_element(By.CLASS_NAME, "card-body")
    assert cardContainer.is_displayed
    print(f"TEST {i * 2 + 2}: {pageTitles[i]} card test passed")
    driver.find_element(By.ID, "logo").click()
    sleep(1)

# test 5: gif test
sleep(3)
busAnimation = driver.find_element(By.ID, 'busAnimation')
assert "animation of a CapMetro bus moving" in busAnimation.get_attribute("alt")
print("TEST 9: bus animation test passed")

# close the driver
driver.quit()