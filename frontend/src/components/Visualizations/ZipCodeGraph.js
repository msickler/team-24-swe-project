import React, { useEffect, useRef, useState } from "react";
import * as Plot from "@observablehq/plot";
import "./GraphStyle.css";

export default function ZipCodeGraph() {
  const [zips, setZips] = useState([]);

  const plotRef = useRef();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("https://api.austin-wheels.me/zipcodes");
        const data = await response.json();
        setZips(data);
        console.log(data)
      } catch (error) {
        console.error("Error fetching zip data:", error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (zips.length > 0) {
      const graph = Plot.plot({
        x: { grid: true, label: "# of Stops" },
        y: { grid: true, label: "# of Locations" },
        color: { legend: true, scheme: "YlGnBu" },
        marks: [
          Plot.ruleY([0]),
          Plot.ruleX([0]),
          Plot.dot(zips, {
            x: "stop_count",
            y: "location_count",
            stroke: "province",
            tip: true,
            r: 4,
            fill: "province"
          })
        ]
      })

      plotRef.current.append(graph)
      console.log("Graph created:", graph);

      return () => {
        // Cleanup code if needed
        graph.remove();
      };
    }
  }, [zips]);

  return (<div ref={plotRef} className='BarGraph'/>);
}
