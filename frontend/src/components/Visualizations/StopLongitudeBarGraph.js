import React, { useEffect, useRef, useState } from "react";
import * as Plot from "@observablehq/plot";
import "./GraphStyle.css";

export default function StopLatitudeBarGraph() {
  const [stops, setStops] = useState([]);
  const plotRef = useRef();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("https://backend.nocarnoproblem.net/api/stops");
        const data = await response.json();
        setStops(data);
        console.log(data)
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (stops.length > 0) {
      const graph = Plot.plot({
        caption: "What is the distance of each destination?",
        x: {
          transform: d => d,
          label: "Location Longitude →"
        },
        marks: [
          Plot.ruleX([-98]),
          Plot.ruleX([-97.5]),
          Plot.ruleY([0]),
          Plot.rectY(stops, {fill: "#2596be", ...Plot.binX({y: "sum"}, {x: "stop_lon"})})
        ]
      })

      plotRef.current.append(graph)
      console.log("Graph created:", graph);

      return () => {
        // Cleanup code if needed
        graph.remove();
      };
    }
  }, [stops]);

  return (<div ref={plotRef} className='BarGraph'/>);
}
