/** @jest-environment jsdom */
import React from "react";
import { BrowserRouter, MemoryRouter } from "react-router-dom";
import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';

// import About from './About';
import Home from './Home';
import Lines from './Lines';
import Stops from './Stops';
import Destinations from './Destinations';
import NavbarComponent from './NavbarComponent';
import ModelOptions from "./ModelOptions";
import About from './About';

// test 1: About page renders correctly
test('renders the About page correctly', () => {
  // Render the About component
  render(
  <BrowserRouter>
      <About/>
  </BrowserRouter>);

  // Use queries from @testing-library/react to make assertions
  const heading = screen.getByText('OUR MISSION');
  const headingTwo = screen.getByText('MEET THE TEAM');

  // Assert that the page contains the expected content
  expect(heading).toBeInTheDocument();
  expect(headingTwo).toBeInTheDocument();
});

// test 2: home/splash page renders correctly
test('renders the Home/Splash page correctly', () => {
  render(
    <BrowserRouter>
      <Home/>
    </BrowserRouter>
  );

  expect(screen.getByText('Lines')).toBeInTheDocument();
  expect(screen.getByText('Stops')).toBeInTheDocument();
  expect(screen.getByText('Destinations')).toBeInTheDocument();
})

// test 3: lines model page renders correctly
test('renders the Lines page correctly', async () => {
  // mock data
  const lines = [
    {
      path: "/bartonCreek",
      global_route_id: "CAPM:7476",
      mode_name: "Bus",
      route_color: "005b8e",
      route_long_name: "Barton Creek / Bull Creek",
      route_short_name: "30",
      route_text_color: "ffffff",
      image: "/",
    }
  ];
    render(
      <BrowserRouter>
      <Lines lines={lines} />
      </BrowserRouter>
    ); // Render the Lines component with mock data
  
    // Use waitFor to wait for an element to appear in the DOM
    await waitFor(() => {
      const title = screen.getByText('Lines');
  
      // Assert that the page title and card components are found in the rendered output
      expect(title).toBeInTheDocument();
    });
});

// test 4: navbar component stops link navigates correctly
test('nav bar correctly navigates to a stops page', () => {
  const { container } = render(
    <MemoryRouter initialEntries={['/home']}>
      <NavbarComponent />
    </MemoryRouter>
  );

  const navigateButton = screen.getByText('Stops');
  fireEvent.click(navigateButton);

  // Check if the component navigated to the correct route
  expect(container.innerHTML).toMatch('/stops');
});

// test 5: nav bar navigates to about page
test('nav bar correctly navigates to about page', () => {
  const { container } = render(
    <MemoryRouter initialEntries={['/home']}>
      <NavbarComponent />
    </MemoryRouter>
  );

  const navigateButton = screen.getByText('About');
  fireEvent.click(navigateButton);

  // Check if the component navigated to the correct route
  expect(container.innerHTML).toMatch('/about');
});

// test __: stops model page renders correctly
// test('renders the Stops page correctly', async () => {
//   // mock data
//   const stops = [
//     {
//       path: "/guad",
//       stop_name: "Guadalupe / West Mall UT",
//       global_stop_id: "CAPM:24170",
//       stop_code: "1042",
//       location_type: "station",
//       wheelchair_boarding: "0",
//       image: "/",
//     }
//   ];

//     render(
//       <BrowserRouter>
//       <Stops stops={stops} />
//       </BrowserRouter>
//     ); // Render the Stops component with mock data
  
//     // Use waitFor to wait for an element to appear in the DOM
//     await waitFor(() => {
//       const title = screen.getByText('Stops');
  
//       // Assert that the page title and card components are found in the rendered output
//       expect(title).toBeInTheDocument();
//     });
// });


// test 6: navbar component lines link navigates correctly
test('nav bar correctly navigates to a lines page', () => {
  const { container } = render(
    <MemoryRouter initialEntries={['/home']}>
      <NavbarComponent />
    </MemoryRouter>
  );

  const navigateButton = screen.getByText('Lines');
  fireEvent.click(navigateButton);

  // Check if the component navigated to the correct route
  expect(container.innerHTML).toMatch('/lines');
});

// test 7: destinations model page renders correctly
test('renders a destinations instance correctly', async() => {
  // mock data
  const destinations = [
    {
      path: "/attHotel",
      business_status: "OPERATIONAL",
      formatted_address: "1900 University Ave, Austin, TX 78705, USA",
      name: "AT&T Hotel and Conference Center",
      rating: "4.5",
      open_now: "1",
      image: "/",
    }
  ];
    render(
      <BrowserRouter>
      <Destinations destinations={destinations} />
      </BrowserRouter>
    ); // Render the Destinations component with mock data
  
    // Use waitFor to wait for an element to appear in the DOM
    await waitFor(() => {
      const title = screen.getByText('Destinations');
  
      // Assert that the page title and card components are found in the rendered output
      expect(title).toBeInTheDocument();
    });
});

// test 8: navbar component destinations link navigates correctly
test('nav bar correctly navigates to a destinations page', () => {
  const { container } = render(
    <MemoryRouter initialEntries={['/home']}>
      <NavbarComponent />
    </MemoryRouter>
  );

  const navigateButton = screen.getByText('Destinations');
  fireEvent.click(navigateButton);

  // Check if the component navigated to the correct route
  expect(container.innerHTML).toMatch('/destinations');
});

// test 9: navbar component renders correctly
test('renders the navbar correctly', async() => {
  render(
    <BrowserRouter>
      <NavbarComponent />
    </BrowserRouter>
    );

    expect(screen.getByText('Lines')).toBeInTheDocument();
    expect(screen.getByText('Stops')).toBeInTheDocument();
    expect(screen.getByText('Destinations')).toBeInTheDocument();
    expect(screen.getByText('About')).toBeInTheDocument();
});

// tst 10: model options (search/filtering on model pages) renders correctly
test('renders model options correctly', async() => {
  const lines = [
    {
      path: "/bartonCreek",
      global_route_id: "CAPM:7476",
      mode_name: "Bus",
      route_color: "005b8e",
      route_long_name: "Barton Creek / Bull Creek",
      route_short_name: "30",
      route_text_color: "ffffff",
      image: "/",
    }
  ];
  
  render(
    <BrowserRouter>
      <ModelOptions 
        sorts={['Route Long Name','Route Short Name','Global Route ID','Route Color','Route Text Color', 'Price']}
        checkbox={false}
        data={lines} />
    </BrowserRouter>
    );

    expect(screen.getByTestId('search')).toBeInTheDocument();
    expect(screen.getByText('1 results found.')).toBeInTheDocument();
});


// mocking images that are imported in components being tested 

// About.js
jest.mock('../assets/trish_headshot.jpg', () => 'mocked-trish-headshot.jpg');
jest.mock('../assets/megan_headshot.jpg', () => 'mocked-megan-headshot.jpg');
jest.mock('../assets/Zach_Headshot.jpeg', () => 'mocked-zach-headshot.jpeg');
jest.mock('../assets/jose_headshot.png', () => 'mocked-jose-headshot.png');
jest.mock('../assets/jared_headshot.jpg', () => 'mocked-jared-headshot.jpg');
jest.mock('../assets/aws-logo.png', () => 'mocked-aws-logo.png');
jest.mock('../assets/docker-logo.png', () => 'mocked-docker-logo.png');
jest.mock('../assets/gitlab-logo.png', () => 'mocked-gitlab-logo.png');
jest.mock('../assets/postman-logo.svg', () => 'mocked-postman-logo.svg');
jest.mock('../assets/react-bootstrap-logo.svg', () => 'mocked-react-bootstarp-logo.svg');
jest.mock('../assets/react-logo.png', () => 'mocked-react-logo.png');
jest.mock('../assets/transit-logo.png', () => 'mocked-transit-logo.png');
jest.mock('../assets/here-logo.svg.png', () => 'mocked-here-logo.svg.png');
jest.mock('../assets/google-maps-api-icon.svg', () => 'mocked-google-maps-api-icon.svg');

// Home.js
jest.mock('../assets/about-page-image.png', () => 'mocked-about-page-image.png');
jest.mock('../assets/bus.gif', () => 'mocked-bus.gif');
jest.mock('../assets/austin-skyline.jpg', () => 'mocked-austin-skyline.jpg');

// ModelOptions.js (used on each model page)
jest.mock('../assets/search_icon.svg.png', () => 'mocked-search_icon.svg.png');

// NavbarComponent.js
jest.mock('../assets/No Car No Problem.png', () => 'mocked-No Car No Problem.png');