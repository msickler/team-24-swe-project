// head shot images
import trish_headshot from '../assets/trish_headshot.jpg';
import megan_headshot from '../assets/megan_headshot.jpg';
import zach_headshot from '../assets/Zach_Headshot.jpeg';
import jose_headshot from '../assets/jose_headshot.png';
import jared_heashot from '../assets/jared_headshot.jpg';

// tool logos
import aws from '../assets/aws-logo.png'
import docker from '../assets/docker-logo.png'
import gitlab from '../assets/gitlab-logo.png'
import postman from '../assets/postman-logo.svg'
import react_boot from '../assets/react-bootstrap-logo.svg'
import react from '../assets/react-logo.png'
import chatgpt from '../assets/chatgpt-logo.png'

// data source logos
import transit_api from '../assets/transit-logo.png'
import here_api from '../assets/here-logo.svg.png'
import google_places from '../assets/google-maps-api-icon.svg'

// header
import header_img from '../assets/about-page-image.png'

// components
import React, { useState, useCallback } from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap';
import Badge from 'react-bootstrap/Badge';
import { Link } from 'react-router-dom';
import axios from "axios";
import '../components/About.css';

const teamInfo = [

    {
        name: "Trish Truong",
        email: "trishtruong@utexas.edu",
        photo: trish_headshot,
        bio: "Trish is a senior in Computer Science at UT Austin, interested in full stack development, hosts hackathons with the Freetail Hackers, and loves binge-watching shows!",
        role: "Full-Stack",
        gitlab: "trishtruong",
        commits: 0,
        issues: 0,
        tests: 10,
    },
    {
        name: "Megan Sickler",
        email: "megansickler@utexas.edu",
        photo: megan_headshot,
        bio: "Megan is a third-year computer science student interested in human-centered UI design. In her free time, she watches lots of basketball and plays for UT’s Women and Non-Binary Ultimate Frisbee club team.",
        role: "Full-Stack",
        gitlab: "msickler",
        commits: 0,
        issues: 0,
        tests: 10,
    },
    {
        name: "Zach Cramer",
        email: "zachcramer@Zachs-MBP.attlocal.net",
        photo: zach_headshot,
        bio: "Zach is in his penultimate year of study at UT Austin. Majoring in computer science, his passions lie in software development and quantitative finance. In his free time, he loves exploring the outdoors with friends and spending way too much time managing his fantasy football team.",
        role: "Full-Stack",
        gitlab: "cramerzach",
        commits: 0,
        issues: 0,
        tests: 10,
    },
    {
        name: "Jose Alonso Rodriguez",
        email: "alonsjous@utexas.edu",
        photo: jose_headshot,
        bio: "Jose is a UTCS undergrad from Mexico City. Jose is interested in exploring different areas of ML as well as getting experience in Software Engineering. Jose enjoys swimming, video games and is currently learning French and Korean.",
        role: "Back-End",
        gitlab: "alonsjous",
        commits: 0,
        issues: 0,
        tests: 10,
    },
    {
        name: "Jared Kahl",
        email: "kahljared4@gmail.com",
        photo: jared_heashot,
        bio: "Jared is a senior at UT Austin majoring in computer science with an emphasis on game development. Jared also mentors highschool students in Unity and likes to learn about making music.",
        role: "Back-End",
        gitlab: "kahljared4",
        commits: 0,
        issues: 0,
        tests: 0,
    }
]

const dataSources = [
    {
        name: "Transit API",
        desc: "Transit gathers raw data for trains, buses, bikes (and more) across 300 cities in 16 countries.",
        link: "https://transitapp.com/apis",
        photo: transit_api
    },
    {
        name: "HERE API",
        desc: "The HERE API offers powerful location-based services, enabling developers to integrate maps, geocoding, routing, and more into their applications.",
        link: "https://developer.here.com/documentation/public-transit/api-reference-swagger.html",
        photo: here_api
    },
    {
        name: "Google Places API",
        desc: "Get location data for over 200 million places, and add place details, search, and autocomplete to your apps.",
        link: "https://developers.google.com/maps/documentation/places/web-service",
        photo: google_places
    }
]

const tools = [
    {
        name: "AWS",
        desc: "A cloud platform that allows software developers to run their applications and store data on the internet.",
        link: "https://aws.amazon.com/",
        photo: aws
    },
    {
        name: "Docker",
        desc: "Docker is a platform for developing, shipping, and running applications in containers.",
        link: "https://www.docker.com/",
        photo: docker
    },
    {
        name: "GitLab",
        desc: "A version control platform used to access repository statistics.",
        link: "https://about.gitlab.com/",
        photo: gitlab
    },
    {
        name: "React",
        desc: "A JavaScript library used to build user interfaces.",
        link: "https://react.dev/",
        photo: react
    },
    {
        name: "React-Bootstrap",
        desc: "A library that combines React and Bootstrap used to create a responsive website.",
        link: "https://react-bootstrap.netlify.app/",
        photo: react_boot
    },
    {
        name: "Postman",
        desc: "An API design and development platform used to create our RESTful API.",
        link: "https://www.postman.com/",
        photo: postman
    },
    {
        name: "ChatGPT",
        desc: "An AI chatbot that is able to help with programming.",
        link: "https://www.chat.openai.com/",
        photo: chatgpt
    }
]

export default function About() {

    const [issuesData, setIssues] = useState([]);
    const [issuesTotal, setTotalIssues] = useState(0);
    const [commitData, setCommits] = useState([{ name: "", commits: "" }]);
    const [commitData2, setCommits2] = useState([{ name: "", commits: "" }]);
    const [commitData3, setCommits3] = useState([{ name: "", commits: "" }]);
    const [commitData4, setCommits4] = useState([{ name: "", commits: "" }]);
    const [commitTotal, setTotalCommit] = useState(0);

    
    React.useEffect(() => {
        const issuesUrl = 'https://gitlab.com/api/v4/projects/50467493/issues?page=1&per_page=200';
        const accessToken = 'glpat-YQz24D1v8B1qaBKtVWbc'; 

        axios.get(issuesUrl, {
            headers: {
                'PRIVATE-TOKEN': accessToken,
            },
        })
        .then(response => {
        console.log(response.data);
        setIssues(response.data);
        setTotalIssues(response.data.length);
        })

        const commitsUrl = 'https://gitlab.com/api/v4/projects/50467493/repository/commits?page=1&per_page=100'

        axios.get(commitsUrl, {
            headers: {
                'PRIVATE-TOKEN': accessToken,
            },
        })
        .then(response => {
            // console.log(response.data);
            setCommits(response.data);
        })

        const commitsUrl2 = 'https://gitlab.com/api/v4/projects/50467493/repository/commits?page=2&per_page=100'
        axios.get(commitsUrl2, {
            headers: {
                'PRIVATE-TOKEN': accessToken,
            },
        })
        .then(response => {
            // console.log(response.data);
            setCommits2(response.data);
        })

        const commitsUrl3 = 'https://gitlab.com/api/v4/projects/50467493/repository/commits?page=3&per_page=100'
        axios.get(commitsUrl3, {
            headers: {
                'PRIVATE-TOKEN': accessToken,
            },
        })
        .then(response => {
            // console.log(response.data);
            setCommits3(response.data);
        })

        const commitsUrl4 = 'https://gitlab.com/api/v4/projects/50467493/repository/commits?page=4&per_page=100'
        axios.get(commitsUrl4, {
            headers: {
                'PRIVATE-TOKEN': accessToken,
            },
        })
        .then(response => {
            // console.log(response.data);
            setCommits4(response.data);
            setTotalCommit(300 + response.data.length);
        })
    }, []);

    const calcIssues = useCallback((gitData, name) => {
        const issues = gitData.filter(issue => issue?.assignee?.username === name)
        return issues.length
    }, []);

    const calcCommits = useCallback((gitData, email) => {
        const commits = gitData.filter(commit => commit?.author_email === email)
        return commits.length
    }, []);
    
        return (
           <>
           <h1 class='page-title'>About</h1>
           <div className='introduction'>
                <div className='intro-content'>
                    <img src={header_img} alt="A Bus"/>
                    <div className='intro-text'>
                        <h4>OUR MISSION</h4>
                        <p>Americans without cars are often underserved 
                            due to the lack of public transportation in American 
                            cities. Through <b>No Car. No Problem.</b>, we hope to support 
                            those without cars in Austin by providing information on bus lines, 
                            bus stops, and popular locations near bus stops. We
                            hope to organize public transportation information, making it easier for 
                            those without cars to move around the city.</p>
                    </div>
                </div>
           </div>
           <div className='meettheteam'>
            <h4>MEET THE TEAM</h4>
            <Row> 
            {
                teamInfo.map((member) => {
                    return (
                        <Col className='colItem'>
                            <Card 
                            className='card-style'
                            text={'dark'}
                            >
                            <Card.Body className="card-body">
                                <Card.Img src={(member.photo +"")} style={{width: '150px', height: '200px', objectFit: 'cover'}}/>
                                <Card.Title className='name'><b>{member.name}</b></Card.Title>
                                <Card.Text className='card-sub'><b>@{member.gitlab} | {member.role} Developer</b></Card.Text>
                                <Badge bg="info">Commits: {calcCommits(commitData, member.email) + calcCommits(commitData2, member.email) + calcCommits(commitData3, member.email) + calcCommits(commitData4, member.email)}</Badge>
                                <Badge bg="info">Issues: {calcIssues(issuesData, member.gitlab)} </Badge>
                                <Badge bg="info">Tests: {member.tests}</Badge>
                                <Card.Text className='card-sub'>{member.bio}</Card.Text>
                            </Card.Body>
                            </Card>
                        </Col>
                    )
                })
            }
            </Row>
            
            <h4>PROJECT RESOURCES</h4>
            <div className='project-specs'>
                <div>
                    <Badge bg="info" id="total-git">
                        Total Number Commits: {commitTotal}
                    </Badge>
                    <Badge bg="info" id="total-git">
                        Total Number Issues: {issuesTotal}
                    </Badge>
                    <Badge bg="info" id="total-git">
                        Total Number Tests: 40
                    </Badge>
                </div>
                <div> 
                    <Link to="https://gitlab.com/msickler/team-24-swe-project">
                        <Button variant="outline-info" className='tool-links'><b>GitLab Repository</b></Button>
                    </Link>
                    <Link to="https://documenter.getpostman.com/view/29865132/2s9YCBvVyB">
                        <Button variant="outline-info" className='tool-links'><b>Postman API Documentation</b></Button>
                    </Link>
                </div>
            </div>


           </div>
           <div className='tools'>
            <h4>TOOLS</h4>
            <Row>
            {
                tools.map((tool) => {
                    return (
                        <Col className='colItem'>
                            <Card
                            className='tool-style'
                            bg={'white'}
                            text={'dark'}
                            >
                                <Card.Body>
                                    <Card.Img src={(tool.photo +"")} style={{width: 'fit'}}/>
                                    <Card.Title className='other-name'>{tool.name}</Card.Title>
                                    <Card.Text className='card-sub'>{tool.desc}</Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    )
                })
            }
            </Row>
           </div>
           <div className='datasources'>
            <h4>DATA SOURCES</h4>
            <Row>
            {
                dataSources.map((source) => {
                    return (
                        <Col className='colItem'>
                            <Card
                            className="data-style"
                            bg={'white'}
                            text={'dark'}
                            >
                                <Card.Body>
                                    <Card.Img className="tool-img" src={(source.photo +"")} />
                                    <Card.Title className='other-name'><Link to={source.link} style={{color: 'rgb(13, 202, 240)'}}>{source.name}</Link></Card.Title>
                                    <Card.Text className='card-sub'>{source.desc}</Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    )
                })
            }   
            </Row>
           </div>
           <div className='disparate-data-result'>
                <h4>RESULT OF DATA INTEGRATION</h4>
                <p>
                The integration of data on public transport routes, stops, and 
                reachable locations has significant benefits for commuters. By consolidating
                 and making this data accessible, it enables commuters to plan their 
                 journeys more efficiently, reducing commute times and enhancing overall 
                 mobility. Commuters can access real-time information on bus and train schedules, 
                 routes, and service disruptions, empowering them to make informed decisions 
                 about their daily commutes. This integration fosters a holistic understanding 
                 of public transport networks, supporting the development of multimodal transportation 
                 solutions, promoting sustainability, and reducing traffic congestion. In essence, the 
                 integration of transport data improves the daily lives of commuters and contributes to 
                 the development of more accessible, efficient, and environmentally friendly urban transportation systems.
                </p>
           </div>
           </> 
        )
}
