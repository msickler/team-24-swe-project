// function inspired from cityscoop.me
import './highlightText.css' 
export function highlightText(text, query) {
  if (text != null) {
    const regex = new RegExp(`(${query})`, 'gi');
    const highlightedText = text.toString().replace(regex, '<span class="highlight">$1</span>');
    return <span dangerouslySetInnerHTML={{ __html: highlightedText }} />;
  }
}
