module.exports = async () => {
    return {
      verbose: true,
      moduleNameMapper: {
        "\\.(css|less|scss|sass)$": "identity-obj-proxy",
        "axios": "axios/dist/node/axios.cjs",
      },
      moduleExports: {
        preset: 'react-native',
        moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
        moduleDirectories: ['./node_modules', 'src'],
        cacheDirectory: '.jest/cache',
        transformIgnorePatterns: [
          '<rootDir>/node_modules/(?!@react-native|react-native)',
        ],
        moduleNameMapper: {
          '^[./a-zA-Z0-9$_-]+\\.svg$': '<rootDir>/tests/SvgStub.js',
          'axios': 'axios/dist/node/axios.cjs',
        },
        setupFiles: ['./node_modules/react-native-gesture-handler/jestSetup.js'],
        modulePathIgnorePatterns: ['<rootDir>/packages/'],
        watchPathIgnorePatterns: ['<rootDir>/node_modules'],
      }
    };
  };
  
