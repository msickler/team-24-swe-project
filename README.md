## Link to Website
http://nocarnoproblem.net/

## Backend Links
<!-- add postman doc link and back end API link -->
API URL: https://backend.nocarnoproblem.net/api/

API Documentation: https://documenter.getpostman.com/view/29865132/2s9YCBvVyB

## Group Information

| Name          | GitLab ID     | EID     |
| ------------- | ------------- | ------- |
| Trish Truong  | @trishtruong  | ttt2448 |
| Megan Sickler | @msickler     | ms85892 |
| Zach Cramer   | @cramerzach   | zc5455  |
| Jared Kahl    | @kahljared4   | jbk966  |
| Jose Alonso   | @alonsjous    | jea3366 |

## Phase Leaders

**Phase Leader Responsibilities**
- Oversee rubric completion
- Lead team meetings
- Check in on team members

| Phase #       | Leader        |
| ------------- | ------------  | 
| One           | Trish Truong  | 
| Two           | Jose Alonso   |  
| Three         | Jared Kahl    | 
| Four          |               | 

## Completion Times

**Phase One**

| Name          | Estimated     | Actual     |
| ------------- | ------------- | ---------- |
| Trish Truong  | 10            | 20         |
| Megan Sickler | 12            | 20         |
| Zach Cramer   | 10            | 13         |
| Jared Kahl    | 10            | 15         |
| Jose Alonso   | 8             | 15         |

**Phase Two**

| Name          | Estimated     | Actual     |
| ------------- | ------------- | ---------- |
| Trish Truong  | 12            | 14         |
| Megan Sickler | 12            | 20         |
| Zach Cramer   | 12            | 35         |
| Jared Kahl    | 13            | 20         |
| Jose Alonso   | 15            | 25         |

**Phase Three**

| Name          | Estimated     | Actual     |
| ------------- | ------------- | ---------- |
| Trish Truong  | 10            | 13         | 
| Megan Sickler | 12            | 24         | 
| Zach Cramer   | 10            | 15         | 
| Jared Kahl    | 13            | 15         | 
| Jose Alonso   | 10            | 20         | 

**Phase Four**

| Name          | Estimated     | Actual     |
| ------------- | ------------- | ---------- |
| Trish Truong  | 8             | 7          | 
| Megan Sickler | 7             | 7          | 
| Zach Cramer   | 6             | 6          | 
| Jared Kahl    | 13            | 20         | 
| Jose Alonso   | 10            | 15         | 

# Git SHA

| Phase #       | Git SHA    								|
| ------------- | ----------------------------------------- | 
| One           | e1116532853813a60c466edd000cdad78da14198  | 
| Two           | 32bce28ecfd96988e8a733029c7cda730ad4cfa7	|  
| Three         | 71245625e2015707d49f5b99cfd50fa05c460632  | 
| Four          | c0bbdf70834755fd500013fc76be15b8b6f87c36  | 
